@extends('layouts.app')
@extends('layouts.head')

@section('content')

<div class="wrapPage">

	<h3>Info page:</h3>

	<div id="companyInfo">
		<?php
	       foreach ($aboutInfo as $about) {
	       
	           echo "Company name:&nbsp" .$about->company_name."<br/>";
	           echo "Description:&nbsp".$about->description."<br/>";
	           echo "Pib:&nbsp".$about->pib."<br/>";
	           echo "Email:&nbsp".$about->email."<br/>";
	       }
        ?>
	</div>

</div>