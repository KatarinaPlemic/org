<?php

namespace App\Http\Controllers;

//use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Company;
//use Symfony\Component\HttpFoundation\Session\Session;

//use Illuminate\Http\Request;
//use App\Http\Requests;
//use App\Http\Controllers\Controller;
use DB;

class CompaniesController extends Controller
{
    
      
    public function index(){        
        
        return view('companies.index');
    }
    
    
    public function selectAllCompanies(){
        
        $companies = DB::table('companies')->get();
        return view('companies.allCompanies', ['companies' => $companies]); 
        //return view('companies.allCompanies');
    }
    
    
    public function seeMoreInfo($id, Request $request){
  
        if(!$request->session()->exists('data')){
            return redirect('');
        }
        
        $aboutInfo = DB::table('companies')->select('company_name','pib','email', 'description')->where('id', $id)->get();
        return view('companies.seeMoreInfo', ['aboutInfo' => $aboutInfo]);
    }
    
    
    public function update($id, Request $request){
        
        if(!$request->session()->exists('data')){
            return redirect('');
        }
        
        $selectedCompany = DB::table('companies')->select('id','company_name','pib','email', 'description')->where('id', $id)->get();
        return view('companies.update', ['selectedCompany' => $selectedCompany]);
    }

    
    public function updateFunc(Request $request){
        
        if(!$request->session()->exists('data')){
            return redirect('');
        }
        
        $id_company = $_GET['id_company'];
        $company_name = $_GET['company_name'];  
        $pib = $_GET['pib'];
        $email= $_GET['email'];
        $description= $_GET['description'];
        
        DB::table('companies')->where('id', $id_company)->update(['company_name' => $company_name, 'pib' => $pib, 'email' => $email, 'description' => $description]);
        
        return view('companies.updatePage');
    }
    
    
    public function delete($id, Request $request){
        
        if(!$request->session()->exists('data')){
            return redirect('');
        }
        
        DB::table('companies')->where('id', '=', $id)->delete();
        return view('companies.delete');
        //DB::delete('delete from companies where id = ?', $id);
        //DB::table('companies')->delete ('companies')->where('id', $id);   
    }
    
   
    //session and login
    
    
    public function login(Request $request){
        
        $tbUsername = $_GET['username'];          //from tb
        $tbPassword = $_GET['password'];          //from tb
        
        $users = DB::table('users')->get();     //from database
        
        foreach ($users as $row) {
            
            $passwords = $row->password;               // passwords from database
            $names = $row->name;                       // names from database
            
            if($tbUsername == $names && $tbPassword == $passwords){
                $usernamePasswordSession = $request->session()->put('data', $request->input());   //set session
            }    
        }
        
        if($request->session()->exists('data')){
            return redirect('selectAllCompanies');
        }
        else{
            return view('companies.wrongLogin');
        }
    }
    

    
    

    
    

}
