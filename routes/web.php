<?php

use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route:get('/about' , function(){
//     return view('pages.about'); 
// });
    

Route::resource('companies','CompaniesController');


Route::get('/','CompaniesController@index');

Route::get('/storeSessionData','CompaniesController@storeSessionData');
Route::get('/getSessionData','CompaniesController@getSessionData');

Route::get('/xxx','CompaniesController@xxx');

Route::get('/selectAllCompanies','CompaniesController@selectAllCompanies');


Route::get('/login','CompaniesController@login');


Route::get('/seeMoreInfo/{id}','CompaniesController@seeMoreInfo');

Route::get('/update/{id}','CompaniesController@update');

Route::get('/updateFunc','CompaniesController@updateFunc');

Route::get('/delete/{id}','CompaniesController@delete');



// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
